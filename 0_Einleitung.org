# Local IspellDict: de
#+STARTUP: showeverything

#+Title:  Wahlpflichtfach 2 — Digitalisierung
#+Author: Prof. Dr. Richard Hirsch
#+EMAIL:  richard.hirsch@th-koeln.de
#+Date:   Wintersemester 2020/21
#+Language: de

#+INCLUDE: "X_OrgStuff.org"

* Organisatorisches

** Seminar

  - dienstags 9:00 Uhr bis 12:20 Uhr
  - /Remote/-Lehrveranstaltung über Zoom
    + Meeting ID: [[https://us02web.zoom.us/meeting/88233848995][882 3384 8995]]
    + Passcode: 286483

** Modulprüfung
   
  - Prüfungsform: Klausur (60 min.)
  - Taxonomiestufe (nach [[https://www.stangl.eu/psychologie/praesentation/lernziele.shtml][Bloom]]): 3

** [[https://miro.com/app/board/o9J_ldEmvEU=/][Semesterprogramm]]

   - Internet
   - Datenformate
   - Datenbanken
   - Algorithmik
   - Sicherheit
   - Künstliche Intelligenz
   - Industrie 4.0

