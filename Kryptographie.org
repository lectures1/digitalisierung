#+TITLE: Digitalisierung – Kryptographie
#+AUTHOR: Prof. Dr. Richard Hirsch
#+DATE: Wintersemester 2020/21

* Org Stuff                                                        :noexport:

** LaTeX
#+LaTeX_CLASS: lecture
#+MACRO: BEAMERSUBJECT Informatik
#+MACRO: BEAMERINSTITUTE Pharmazeutische Chemie
#+BEAMER_FRAME_LEVEL: 3
#+OPTIONS: toc:nil H:3
#+LaTeX_HEADER: \usepackage{booktabs}
#+LaTeX_HEADER: \usepackage{amsmath}
#+LaTeX_HEADER: \usepackage{tikz}
#+LaTeX_HEADER: \usetikzlibrary{arrows}
#+LaTeX_HEADER: \newcommand{\tablesize}{\scriptsize}
#+LaTeX_HEADER: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Gliederung}\tableofcontents[currentsection]\end{frame}}
#+LaTeX_HEADER: \AtBeginSubsection[]{\begin{frame}<beamer>\frametitle{Gliederung}\tableofcontents[currentsection,currentsubsection]\end{frame}}


* Verschlüsselung und Authentifizierung

** Motivation

*** Warum Verschlüsselungsverfahen

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
  \begin{tikzpicture}[every node/.style={draw}]
    \node (n) at ( 0, 0) {Netzwerk};
    \node (A) at (-4, 0) {Alice};
    \node (B) at ( 4, 0) {Bob};
    \node (E) at ( 0,-3) {Eve};
    \draw[->] (A) -- (n);
    \draw[->] (n) -- (B);
    \draw<2>[->] (n) -- (E);
  \end{tikzpicture}
#+END_EXPORT
#+END_CENTER

\pause\medskip

- Eve soll nicht wissen, was sich Alice und Bob mitteilen.
- Bob möchte sichergehen, dass eine Nachricht auch wirklich von Alice ist.
- Eve soll nicht erfahren, dass Alice und Bob überhaupt kommunizieren.


** Verschlüsselung

*** One-Time-Pad

- Jedes Bit des Klartextes wird mit einem Bit des Schlüssels mittels
  XOR kodiert:

  | Klartext   | 0 | 1 | 1 | 0 | 0 | 1 | \dots |
  | Schlüssel  | 1 | 0 | 1 | 1 | 0 | 0 | \dots |
  |------------+---+---+---+---+---+---+-------|
  | Geheimtext | 1 | 1 | 0 | 1 | 0 | 1 | \dots |


- Entschlüsselung funktioniert genauso:

  | Geheimtext | 1 | 1 | 0 | 1 | 0 | 1 | \dots |
  | Schlüssel  | 1 | 0 | 1 | 1 | 0 | 0 | \dots |
  |------------+---+---+---+---+---+---+-------|
  | Klartext   | 0 | 1 | 1 | 0 | 0 | 1 | \dots |


- Der Schlüssel muss genauso lang sein, wie der Klartext
- Das Verfahren ist so sicher wie die Schlüsselübermittlung

*** Symmetrische Verschlüsselung

- Funktionsweise wie One-Time-Pad: 
  Ver- und Entschlüsselung mit dem gleichen Schlüssel
- ein kurzer Schlüssel wird immer wieder verwendet, um Teilstücke des
  Klartextes zu verschlüsseln.

Beispiele: 

- *AES* (Advanced Encryption Standard) oder Rijndael: der
  US-amerikanische Verschlüsselungsstandard
# , Nachfolger des DES; von Joan Daemen und Vincent Rijmen entwickeltes Blockverschlüsselungsverfahren
- *DES* (Data Encryption Standard) oder Lucifer: bis zum Oktober 2000
  der Verschlüsselungsstandard der USA. 
# Lucifer, das Verfahren, wurde 1974 von IBM entwickelt. Die Version für Privatanwender heißt Data Encryption Algorithm (DEA).
- *Triple-DES*: eine Weiterentwicklung des DES-Verfahrens; dreimal langsamer, aber um Größenordnungen sicherer
- *IDEA* (International Data Encryption Algorithm): ein 1990 an der ETH Zürich entwickeltes Blockverschlüsselungsverfahren; Software-patentiert von Ascom Systec; Anwendung in PGP
# - *Blowfish*: 1993 von Bruce Schneier entwickeltes Blockverschlüsselungsverfahren, unpatentiert
# - *QUISCI* (Quick Stream Cipher): sehr schnelles Stromverschlüsselungsverfahren von Stefan Müller 2001 entwickelt, unpatentiert
# - *Twofish*: Blockverschlüsselungsverfahren, vom Counterpane Team; wird u.a. in Microsoft Windows eingesetzt.
# - *CAST-128, CAST-256*: Blockverschlüsselungsverfahren von Carlisle M. Adams, unpatentiert
# - *RC2, RC4, RC5, RC6* („Rivest Cipher“): mehrere Verschlüsselungsverfahren von Ronald L. Rivest
# - *Serpent*: Blockverschlüsselungsverfahren von Ross Anderson, Eli Biham und Lars Knudsen, unpatentiert


*** COMMENT IDEA

- acht Transformationen mit jeweils sechs 16-Bit-Teilschlüsseln
- eine Transformation mit vier 16-Bit-Teilschlüsseln


*** Problem Schlüsselaustausch

- Problem bei symmetrischen Verschlüsselungsverfahren:
  Schlüsselaustausch
- sicherer Schlüsselaustausch über öffentliche Kanäle ist möglich
  (Diffie-Hellman-Protokoll), Cave: Man muss sicherstellen, dass man
  mit der richtigen Person kommuniziert!

*** Schlüsselvereinbarung nach Diffie-Hellman

1. Teilnehmer A und B vereinbaren eine (große) Primzahl $p$ und eine Hilfszahl $g$.
   + Vereinbarung über öffentliche Kanäle möglich,
   + Für $g$ gibt es bestimmte Voraussetzungen, damit das Verfahren
     sicher ist 
     (Erzeugendes Element der zyklischen Gruppe
     $\mathbb{Z}_p\backslash\{0\}$
     oder einer Untergruppe mit Primzahlordnung).
2. 
   + Teilnehmer A wählt geheim eine Zufallzahl $a$ und berechnet 
     $h_A = g^a \mod p$,
   + Teilnehmerin B wählt geheim eine Zufallzahl $b$ und berechnet 
     $h_B = g^b \mod p$.
3. Teilnehmer tauschen $h_A$ und $h_B$ (über öffentliche Kanäle) aus.
4. 
   + Teilnehmer A berechnet $h_B^a$,
   + Teilnehmerin B berechnet $h_A^b$.
   + Beide erhalten das gleiche Ergebnis $K$, denn es ist:
     $$ h_B^a \mod p = g^{ba} \mod p = g^{ab} \mod p = h_A^b \mod
     p$$
   + $K$ kann als (geheimer) Schlüssel für eine symmetrische
     Verschlüsselung verwendet werden. 


*** Sicherheit des Diffie-Hellman-Verfahrens

    Ein Angreifer müsste aus $g^a
    \mod p$ und $g^b \mod p$ das Geheimnis $g^{ab} \mod p$
    berechnen können (/diskreter Logarithmus/); 
    das ist für große $p$ nicht effizient möglich.
    

*** Beispiel für eine Diffie-Hellman Schlüsselvereinbarung

| Größe | praktische Anwendung | Beispiel |
|-------+----------------------+----------|
|       | <r>                  |          |
| $p$   | 2048 Bit             |       47 |
| $g$   | >224 Bit             |        5 |


\pause \bigskip

| $a$ | $g^a \mod p$ |   | $b$ | $g^b \mod p$ |
|-----+---------------+---+-----+---------------|
|  24 |            42 |   | 35  | 29            | 

\pause \bigskip

| $29^{24} \mod 47$ |   | $42^{35} \mod 47$ | 
|--------------------+---+--------------------+-
|                 18 |   |                 18 | 
 


*** COMMENT Diskreter Logarithmus

  

*** Asymmetrische Verschlüsselung

- Probleme bei der symmetrischen Verschlüsselung:
  + Für $N$ Teilnehmer muss jeder Teilnehmer $N-1$ geheime Schlüssel
    verwalten
  + Schlüsselaustausch notwendig
  + Vertrauen in den Kommunikationspartner notwendig
    (Schlüsselweitergabe)

\pause \bigskip

Asymmetrische Verschlüsselung (/public key encryption/)

- Ver- und Entschlüsselung mit zwei verschiedenen Schlüsseln
  + zur Verschlüsselung: öffentlich
  + zur Entschlüsselung: geheim



*** RSA-Verfahren

1. Man wählt zwei (große) Primzahlen $p$ und $q$,
2. berechnet $n = p \cdot q$,
3. berechnet $\phi(n) = (p - 1)(q - 1)$,
4. wählt $e$ teilerfremd zu $\phi(n)$,
5. ermittelt $d$ mit $ed \bmod \phi(n) = 1$.

\medskip
| $d$    | privater, (geheimer) Schlüssel |
| $e, n$ | öffentlicher Schlüssel         | 


*** RSA-Verschlüsselung

Verschlüsselung einer Nachricht $m$ mit $e$ und $n$:
$$c = m^e \mod n$$

\pause \medskip

Entschlüsselung mit dem privaten Schlüssel $d$:
$$ c^d \mod n = m$$


\pause \bigskip

Funktionsprinzip (Satz von Euler):
$$ ed \equiv 1 \bmod \phi(n),\qquad m^{ed} \mod n = m^1$$


** Authentifizierung

*** Authentifizierung

- Teilnehmerauthentifizierung
- Nachrichtenauthentifizierung


*** Teilnehmerauthentifizierung

Eindeutige Kennzeichnung eines Teilnehmers:

- biologische Merkmale (Aussehen, Fingerabdruck, Muttermale)
- Besitz eines einzigartigen Objekts
- einzigartiges Wissen (Geheimnis)


*** Nachrichtenauthentifizierung

- Authentifizierung von Dokumenten
  + Unterschrift
  + Echtheitsmerkmale (Banknoten)

*** Nachrichtenauthentifizierung über asymmetrische Verschlüsselung

Signieren einer Nachricht $m$ mit $d$ (geheimer Schlüssel) und $n$:
$$s = m^d \mod n$$

\pause \medskip

Verifizierung (Entschlüsselung) mit dem öffentlichen Schlüssel $e$:
$$ s^e \mod n = m$$


\pause \bigskip

- In der Praxis wird nicht die ganze Nachricht chiffriert, sondern nur
  ein Hash-Wert (Fingerabdruck),
- Der Sender A ermittelt den Hashwert der Nachricht und verschlüsselt ihn mit seinem
  privaten Schlüssel, das Ergebnis wird als Signatur mit der
  Nachricht verschickt.
- Der Empfänger ermittelt den Hashwert der Nachricht und
  vergleicht ihn mit der entschlüsselten Signatur (öffentlicher Schlüssel).


** Steganographie

*** Steganographie

verborgene Übermittlung von Informationen

- Sprache
- Sport
- Kopierschutz
- Täuschungsversuche


*** Text als Übertragungsmedium

Hallo Jörg, 

es gibt immer noch Probleme mit dem Projekt für die
Fachhochschule. Bitte kommt morgen unbedingt zur Projektbesprechung!
Falls Ihr nicht könnt, gebt rechtzeitig Bescheid. Wir müssen das aber
in der großen Runde besprechen, sonst müssten wir noch einmal in
Klausur gehen. Nächstes Mal ist dann aber der Uwe wieder dran. Hoffen
wir, dass alles gut geht.

Viele Grüße
Anton

\pause\bigskip
Schlüssel 15, 22, 31, 32, 42, 52


*** Text als Übertragungsmedium

Schlüssel 15, 31, 32, 42, 52:

\bigskip\bigskip
Hallo Jörg, 

es gibt immer noch Probleme mit dem Projekt für die
Fachhochschule. Bitte *kommt* morgen unbedingt zur Projektbesprechung!
Falls Ihr nicht könnt, gebt rechtzeitig Bescheid. Wir müssen das aber
*in* *der* großen Runde besprechen, sonst müssten wir noch einmal in
*Klausur* gehen. Nächstes Mal ist dann aber der Uwe wieder *dran*. Hoffen
wir, dass alles gut geht.

Viele Grüße
Anton



* COMMENT Blockchain-Technologien

* OpenPGP / GnuPG                                                  :noexport:

- [[https://hashrocket.com/blog/posts/encryption-with-gpg-a-story-really-a-tutorial][Encryption with gpg, a story (really, a tutorial)]]

- [[http://www.hauke-laging.de/sicherheit/openpgp.html][Einführung in die Funktionsweise von OpenPGP / GnuPG (gpg)]]  

- [[https://futureboy.us/pgp.html][Alan Eliasen: GPG Tutorial]]  
