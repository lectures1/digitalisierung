#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" exploring the PKCS1_v1_5 module of the crypto library """

from binascii import hexlify, unhexlify

from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.exceptions import InvalidSignature

# set global parameters
padding = padding.PSS(
    mgf=padding.MGF1(hashes.SHA256()),
    salt_length=padding.PSS.MAX_LENGTH
)
algorithm = hashes.SHA256()

# generate keys
key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=512
)
private = key.private_bytes(
    encoding=serialization.Encoding.DER,
    format=serialization.PrivateFormat.TraditionalOpenSSL,
    encryption_algorithm=serialization.NoEncryption()
)
public_raw = key.public_key().public_bytes(
    encoding=serialization.Encoding.DER,
    format=serialization.PublicFormat.SubjectPublicKeyInfo
)
public = hexlify(public_raw).decode('ascii')

# create signature
def sign_hex(message, private_key_DER, password=None):
    signer_key = serialization.load_der_private_key(private, password=password)
    signature_raw = signer_key.sign(message, padding, algorithm)
    return hexlify(signature_raw).decode('ascii')

message = b'To be signed'
signature = sign_hex(message, private)

# verify signature
def is_valid(message, signature, public_key_DER_hex):
    pkey = unhexlify(public_key_DER_hex)
    verifier_key = serialization.load_der_public_key(unhexlify(public))
    try:
        verifier_key.verify(unhexlify(signature), 
                            message, padding, algorithm)
    except InvalidSignature:
        return False
    return True

def verify(message, signature, public_key_DER_hex):
    if is_valid(message, signature, public_key_DER_hex):
        print('Signature is valid.')
    else:
        print('Signature is NOT valid!')

print('The following signature should be valid:')        
verify(message, signature, public)
print('The following signature should be invalid:')        
verify(b'invalid' + message, signature, public)
