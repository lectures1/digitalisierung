#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Client class for the blockchain application """

from binascii import hexlify

from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.hazmat.primitives import hashes, serialization

from transaction import Transaction

class Client:
    
    def __init__(self):
        """
        create a new client, the client is identified by
        their public key.
        """
        
        self._private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=1024,
            backend=None
        )
        self._public_key = self._private_key.public_key()

    def export_public_key(self, format='PEM'):

        if format == 'PEM':
            bytes = self._public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            return bytes.decode('ascii')
        elif format == 'DER':
            bytes = self._public_key.public_bytes(
                encoding=serialization.Encoding.DER,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            return hexlify(bytes).decode('ascii')
        else:
            raise f"unknown public key format '{format}'"
        return None

    def sign_transaction(self, t):

        signature = self._private_key.sign(
            str.encode(t.hash()),
            padding=Transaction.padding,
            algorithm=Transaction.algorithm
        )
        t.signature = hexlify(signature).decode('ascii')

        return t.signature

    def create_signed_transaction(self, recipient_idty, value):
        """
        create a transaction to recipient and sign it

        :param recipient_idty: public signature of recipient
        :param value: amount to be transferred
        """

        t = Transaction(self.identity, recipient_idty, value)
        self.sign_transaction(t)

        return t
    
    @property
    def identity(self):
        return self.export_public_key(format='DER')


def test_client():
    richard = Client()
    dagmar= Client()
    for c in [richard, dagmar]:
        print('Identity:', c.identity)
        print(c.export_public_key(format='PEM'))

if __name__ == '__main__':
    test_client()
