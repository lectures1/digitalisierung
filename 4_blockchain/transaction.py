#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Transaction class for the blockchain application """

from binascii import hexlify, unhexlify
from datetime import datetime
from collections import OrderedDict
from textwrap import wrap

from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.exceptions import InvalidSignature

class Transaction:

    transaction_pool = []

    padding = padding.PSS(
        mgf=padding.MGF1(hashes.SHA256()),
        salt_length=padding.PSS.MAX_LENGTH
    )
    algorithm = hashes.SHA256()

    @classmethod
    def hash_hexdigest(cls, s):
        """
        computes SH256 digest

        :param cls: class identifier
        :param s:   string to be digested
        :returns:   ascii encodes hexdigest
        """

        digest = hashes.Hash(hashes.SHA256())
        digest.update(s)
        return hexlify(digest.finalize()).decode('ascii')


    def __init__(self, sender_idty, recipient_idty, value):
        """
        create a new transaction, the signature is valid if
        it was issued by the private key of the sender

        :param sender_idty:    identity of the sender
        :param recipient_idty: identity of the recipient
        :param value:          amount of THCoin to be transferred
        """
        
        self.sender = sender_idty
        self.recipient = recipient_idty
        self.value = value
        self.timestamp = datetime.now()
        self.signature = None


    def to_dict(self):
        """ this dictionary is used for the creation of the hash """
        return OrderedDict({
            'sender':    self.sender,
            'recipient': self.recipient,
            'value':     self.value,
            'time':      self.timestamp
        })


    def hash(self):
        transaction_string = str(self.to_dict()).encode('utf8')
        return Transaction.hash_hexdigest(transaction_string)

    def has_valid_signature(self):
        """ 
        The transactions's signature is verified against
        the public key of the sender (i. e. their identity)

        :param key_der: self.sender._public_key (in 'DER' format
        """

        public_key = unhexlify(self.sender)
        verifier_key = serialization.load_der_public_key(public_key)
        try:
            verifier_key.verify(
                unhexlify(self.signature), 
                str.encode(self.hash()),
                Transaction.padding, 
                Transaction.algorithm
            )
        except InvalidSignature:
            return False
        return True

    def queue(self):
        Transaction.transaction_pool.append(self)
        

    def dump(self, line_width=65):
        print('-'*4, 'Transaction:', '-'*58)
        for k in ('signature', 'sender', 'recipient', 'timestamp', 'value'):
            v = getattr(self, k)
            for vv in wrap(str(v), line_width):
                print(f"{k+':':10} {vv}")

def test_transaction():    
    from client import Client
 
    richard = Client()
    dagmar = Client()

    # t1 is a valid transaction:
    t1 = richard.create_signed_transaction(dagmar.identity, 10)

    # t2 is a fake (Richard signs transfer from Dagmar's account
    t2 = Transaction(dagmar.identity, richard.identity, 100.0)
    richard.sign_transaction(t2)

    t1.dump()
    t2.dump()
    print('Transaction 1:', 'valid' if t1.has_valid_signature() else 'not valid')
    print('Transaction 2:', 'valid' if t2.has_valid_signature() else 'not valid')
    
    
if __name__ == '__main__':

    test_transaction()

 
