#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" blockchain class for the blockchain application """

from client import Client
from transaction import Transaction
from block import Block

class Chain:

    def __init__(self, recipient_idty, value, difficulty = 4):
        """ 
        the blockchain is initialized with block 0, the Genesis
        block. The Genesis block holds only one transaction: the
        transfer of <value> to <recipient_idty> 

        :param recipient_idty:  identity of recipient of the first transfer
        :param value:           amount to be transferred to recipient
        :param difficulty:      difficulty of mining a block
        """

        genesis = Client()
        t0 = genesis.create_signed_transaction(recipient_idty, value)
        b0 = Block(0, [t0], prev_hash=0, difficulty=difficulty)
        b0.mine()
        assert b0.is_valid()

        self.id = genesis.identity
        self.difficulty = difficulty
        self.chain = [ b0 ]

    def is_valid(self):
        """
        the blockchain is valid if
        1) all blocks in `self.chain` are valid,
        2) every block (except the genesis block) contains the
           correct hash value of its previous block
        """

        blocks_valid = all(b.is_valid for b in self.chain)
        links_valid = all(b0.hash() == b1.previous_block_hash 
                          for b0, b1 in zip(self.chain, self.chain[1:]))

        return blocks_valid and links_valid

    def new_block(self, transactions):
        """ 
        generate a new block for the chain 
        """

        return Block(
            index        = len(self.chain),
            transactions = transactions,
            prev_hash    = self.chain[-1].hash(),
            difficulty   = self.difficulty
        )
        

    def append(self, block):
        """
        appends a valid (mined) block to the blockchain
        
        :param block:     block to be added
        """

        assert(block.is_valid())
        self.chain.append(block)


    def dump(self):
        print("Number of block in the chain: ", str(len(self.chain)))
        for block in self.chain:
            block.dump()

    def get_transactions(self):
        transactions = [t for block in self.chain 
                        for t in block.verified_transactions]
        assert all(t.has_valid_signature() for t in transactions)
        return transactions

    def print_transactions(self):
        print('Transactions:')
        print('Timestamp                  | Sender    | Recipient | Amount')
        print('---------------------------+-----------+-----------+-------')
        for t in self.get_transactions():
            sender_ID = f'{t.sender[58:62]}-{t.sender[62:66]}'
            recipient_ID = f'{t.recipient[58:62]}-{t.recipient[62:66]}'
            timestamp = t.timestamp.strftime("%Y-%m-%dT%H:%M:%S:%f")
            print(f"{timestamp} | {sender_ID} | {recipient_ID} | {t.value:6}")

    def print_balances(self):

        balances = {}
        genesis = self.chain[0].verified_transactions[0].sender

        for t in self.get_transactions():
            sender_ID, recipient_ID = [ f'{getattr(t, c)[58:62]}-{getattr(t, c)[62:66]}'
                                        for c in ['sender', 'recipient'] ]
            if t.sender == genesis:
                sender_ID = 'genesis'
            if sender_ID not in balances:
                balances[sender_ID] = 0
            if recipient_ID not in balances:
                balances[recipient_ID] = 0
            balances[sender_ID]-= t.value
            balances[recipient_ID]+= t.value
        del balances['genesis']

        print('final balance:')
        for id in balances.keys():
            print(f'{id:9}', end=' ')
        print()
        for value in balances.values():
            print(f'{value:9}', end=' ')
        print()
        
def test_chain():

    print('creating clients ...')
    president = Client()
    hirsch = Client()
    students = [Client() for _ in range(8)]

    print('creating chain ...')
    THCoin = Chain(president.identity, 1000, 0)

    print('creating transactions ...')
    t1 = president.create_signed_transaction(hirsch.identity, 100)
    ts = [hirsch.create_signed_transaction(s.identity, 10) for s in students]

    print('creating blocks')
    b1 = THCoin.new_block([t1] + ts[:2])
    print('mining block 1 ...')
    b1.mine()
    THCoin.append(b1)

    b2 = THCoin.new_block(ts[2:])
    # b2 has not been mined!
    try:
        THCoin.append(b2)
    except AssertionError:
        print('block 2 is not valid, mining it ...')
        b2.mine()
        THCoin.append(b2)


    print('creating and mining block 3 ...')
    t3a = students[0].create_signed_transaction(students[3].identity, 5)
    t3b = students[3].create_signed_transaction(students[5].identity, 2)
    t3c = students[1].create_signed_transaction(students[5].identity, 1)
    
    b3 = THCoin.new_block([t3a, t3b, t3c])
    b3.mine()
    THCoin.append(b3)
    

    THCoin.dump()
    print('THCoin is valid:', THCoin.is_valid())

    # corrupt the chain (remove a transaction)
    # THCoin.chain[1].verified_transactions.pop(0)
    # print('corrupted chain is valid:', THCoin.is_valid())

    THCoin.print_transactions()
    THCoin.print_balances()

if __name__ == '__main__':
    test_chain()
