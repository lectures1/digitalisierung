#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Abstract class for objects with self hashing abilities """

from binascii import hexlify, unhexlify
from datetime import datetime
from collections import OrderedDict
from textwrap import wrap

from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.exceptions import InvalidSignature

class Hasher:

    padding = padding.PSS(
        mgf=padding.MGF1(hashes.SHA256()),
        salt_length=padding.PSS.MAX_LENGTH
    )
    algorithm = hashes.SHA256()

    def __init__(self, prop1, prop2):
        """
        create a new self hashing object

        self.hashed is a list of property names that are used to compute the hash
        """

        self.prop1  = prop1
        self.prop2  = prop2

        self.hashed = ["prop1", "prop2"]


    def serialize(self):
        """ a bytesstring representaton of all the relevant object properties """

        s = str( [getattr(self, p) for p in self.hashed] )
        return s.encode('utf-8')


    def hash(self):
        """
        computes SH256 digest for the objekct

        :returns:   ascii encoded hexdigest
        """

        digest = hashes.Hash(hashes.SHA256())
        digest.update(self.serialize())
        return hexlify(digest.finalize()).decode('ascii')


    def dump(self, line_width=65):
        print('-'*5, self.__class__.__name__, '-'*62)
        for k in self.hashed:
            v = getattr(self, k)
            for vv in wrap(str(v), line_width):
                print(f"{k+':':10} {vv}")
        print(f"{'Hash:':10} {self.hash()}")

def test_hasher():    

    h = Hasher(4, "vier")
    h.dump()
    
if __name__ == '__main__':

    test_hasher()

 
